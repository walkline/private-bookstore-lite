'use strict'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				initView(localStorage.settings_paging_limit);

				break;
			case "backbutton":
				navigator.app.exitApp();

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();

				break;
		}
	}
};

var uid = localStorage.uid;

$("#book_header").click(function () {
	$(this).next().slideToggle();
});

//响应查询按钮
$("#query2-btn").click(function () {
	startQuery2();
});

function initView(limit, page) {
	limit = limit || 5;
	page = page || 0;

	if (!uid) {
		location.href = "index.html";
	} else {
		$("#book_lists").hide();

		$.post (getApiUrl("catalogfilters"), {}, function (result) {
			$("#catalog_filters").html(generateFilterLists(result));
		});

		refreshManageLists(limit, page);

		$("#search").focus();
	}
};

function refreshManageLists(limit, page) {
	limit = limit || 5;
	page = page || 0;

	$.post (getApiUrl("getbooklists"), {uid: uid, limit: limit, page: page}, function (result) {
		$("#book_lists").html(generateLists(result));

		$(".s_list").each(function () {
			$(this).click(function () {
				confirm2delete($(this));
			});
		});
	});
}

function generateLists(jsonObj) {
	var output = "";

	if (jsonObj.lists.length > 0) {
		//小说列表
		$.each(jsonObj.lists, function(index, list) {
			output += '<div id=book' + list.bid + ' class="s_list">\n';
			output += '<div class="pt-name">' + list.title + '</div>\n';
			output += '<div class="pt-author">' + list.author + '</div>\n';
			output += '<div class="pt-bid">' + list.bid + '</div>\n'
			output += '</div>\n';
		});

		//翻页区
		if (!$.isEmptyObject(jsonObj.paging)) {
			var paging = jsonObj.paging;

			output += '<div class="s_list" style="height:30px;margin-top:0px;">\n';

			for (var count = 0; count < paging.paging_counts; count++) {
				var active_a = count == paging.current_page ? 'style="background-color:black;"' : "";
				var active_span = count == paging.current_page ? 'style="color:white;"' : "";

				output += '<a class="nav_pages" ' + active_a + ' href="javascript:void(0);" onclick="initView(' + localStorage.settings_paging_limit + ', ' + count + ')">\n';
				output += '<span class="nav_span" ' + active_span + '>' + (count + 1) +'</span>\n';
				output += '</a>';
			}

			output += '</div>';
		}
	} else {
		output = '(空)';
	}

	return output;
}

function generateFilterLists(jsonObj) {
	var output = "";

	if (jsonObj.catalog_filters.length > 0) {
		$.each(jsonObj.catalog_filters, function(index, filter) {
			output += '<option value="' + index + '">' + filter + '</option>\n';
		});
	}

	return output;
}

function startQuery2() {
	var search = $("#search").val();
	var filter = $("#catalog_filters").val();

	$("#result-output").hide();

	if (search.length >= 2) {
		$("#query2-btn").attr("disabled", true);

		$.post (getApiUrl("bookinfo"), {search: search, filter: filter}, function (result) {
			$("#result-output").html(generateSearchResultList(result));
			
			$("#result-output").slideDown();
			$("#query2-btn").attr("disabled", false);

			$(".search_list").each(function () {
				$(this).click(function () {
					confirm2append($(this));
				});
			});
		});
	} else {
		$("#search").focus();
	}
}

function generateSearchResultList(jsonObj) {
	var output = "";

	if (!jsonObj.error_code) {
		if (jsonObj.lists.length > 0) {
			$.each(jsonObj.lists, function(index, list) {
				output += '<div id="book' + list.bid + '" class="search_list">\n';
				output += '<div class="pt-bid">' + list.bid + '</div>\n';
				output += '<div class="pt-name">' + list.title + '</div>\n';
				output += '<div class="pt-search">' + list.description + '</div>\n';
				output += '<div class="pt-search">作者：<author>' + list.author + '</author></div>\n';
				output += '<div class="pt-search">类型：' + list.type + '</div>\n';
				output += '<div class="pt-search">更新时间：' + list.update_time + '</div>\n';
				output += '<div class="pt-search">最新章节：' + list.new_chapter + '</div>\n';
				
				output += '</div>\n';
			});
		} else {
			output = "无结果"
		}

		$("#search").val("");
	} else {
		output = jsonObj.error_msg;
	}

	return output;
}

//响应列表删除命令
function confirm2delete(obj) {
	var title = $(obj).children(".pt-name").text();

	localStorage.selectedItemId = $(obj).attr("id");

	navigator.notification.confirm ("确定删除《" + title + "》吗？", onConfirm2Delete, "删除小说", ['删除', '取消']);
}

function onConfirm2Delete(btnIndex) {
	var objId = localStorage.selectedItemId;
	var bid = $("#"+objId).children(".pt-bid").text();

	if (objId && bid && btnIndex == 1) {
		$.post (getApiUrl("removebook"), {uid: uid, bid: bid, include_history: localStorage.settings_clean_history}, function (result) {
			if (result.result) {
				$("#"+objId+".s_list").remove();

				if ($("#book_lists").children().length == 0) {
					$("#book_lists").html("(空)");
				}
			} else {
				alert("删除失败！");
			}
		});
	}
}

//响应列表添加命令
function confirm2append(obj) {
	var title = $(obj).children(".pt-name").text();

	localStorage.selectedItemId = $(obj).attr("id");

	navigator.notification.confirm ("是否添加《" + title + "》？", onConfirm2Append, "添加小说", ['取消', '添加']);
}

function onConfirm2Append(btnIndex) {
	var objId = localStorage.selectedItemId;
	var bid = $("#"+objId).find(".pt-bid").text();
	var title = $("#"+objId).find(".pt-name").text();
	var author = $("#"+objId).find("author").text();

	if (bid && title && author && btnIndex == 2) {
		$.post (getApiUrl("appendbook"), {uid: uid, bid: bid, title: title, author: author}, function (result) {
			if (!result.error_code) {
				if (localStorage.settings_clean_search_result === "true") {
					$("#result-output").hide();
				}

				$("#book"+bid+".s_list").remove();
				//refreshManageLists(localStorage.settings_paging_limit);
				var new_list = "";

				new_list += '<div id=book' + bid + ' class="s_list" style="background: lightblue;">\n';
				new_list += '<div class="pt-name">' + title + '</div>\n';
				new_list += '<div class="pt-author">' + author + '</div>\n';
				new_list += '<div class="pt-bid">' + bid + '</div>\n'
				new_list += '</div>\n';

				if ($("#book_lists").children().length == 0) {
					$("#book_lists").html(new_list);
				} else {
					$("#book_lists").prepend(new_list);
				}

				$(".s_list").off("click");
				$(".s_list").each(function () {
					$(this).click(function () {
						confirm2delete($(this));
					});
				});

				if ($("#book_lists").is(":hidden")) {
					$("#book_lists").slideDown();
				}
			} else {
				$("#result-output").html(result.error_msg);
			}
		});
	}
}
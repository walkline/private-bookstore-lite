'user strick'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				initView();

				break;
			case "backbutton":
				location.href = "index.html";

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();
				
				break;
		}
	}
};

var url;
var uid = localStorage.uid;
var page = $.url().param("page");
var bid = $.url().param("bid");
var title = $.url().param("title");

$("#homepage").click(function () {
	location.href = "index.html";
});

$("#jump").mousedown(function (event) {
	$("#jumpto").css("height", $(window).height() / 3);
	$("#jumpto").slideToggle();
	$("#jumpto").focus();
	event.stopPropagation();
});

$(".changeorder").click(function () {
	if (localStorage.settings_chapter_sort_order === "asc") {
		localStorage.settings_chapter_sort_order = "desc";
	} else {
		localStorage.settings_chapter_sort_order = "asc";
	}

	getBookHomeChapters();
});

$("#catalog").click(function () {
	getBookHomeChapters();
});

$(document).on("mousedown scroll", function () {
	$("#jumpto").slideUp(300);
});

function initView() {
	if (!uid) {
		location.href = "index.html";
	} else {
		//$("title").html(title);
		$(".ptm-title").html(title);

		getBookHomeChapters();
	}
}

function getBookHomeChapters (page) {
	page = page || 1;

	$.post (getApiUrl("bookchapters"), {bid: bid, page: page, new_chapter: localStorage.settings_show_new_chapter, order: localStorage.settings_chapter_sort_order}, function (result) {
		generateChapterLists(result);
	});
}

function generateChapterLists(jsonObj) {
	if (!jsonObj.error_code) {
		//显示最新章节目录列表
		if (jsonObj.new_chapters.count > 0) {
			var output_new_chapters = "";
			var chapter_limits = localStorage.settings_new_chapter_limit;
			
			$.each(jsonObj.new_chapters.lists, function (index, chapter) {
				if (index >= chapter_limits) {return false;}

				output_new_chapters += '<li>\n';
				output_new_chapters += '<a href="article.html?bid=' + chapter.bid + '&article_id=' + chapter.article_id + '&title=' + title + '">' + chapter.article_title + '</a>\n';
				output_new_chapters += '</li>\n';
			});

			if (jsonObj.new_chapters.update_time) {
				$("#update_time").html(" (" + jsonObj.new_chapters.update_time + ")");
			}

			$("#new_chapters>ul").html(output_new_chapters);
			$("#new_chapters").slideDown();
			$("#chapters>ul").css("padding-top", "15px");
		} else {
			$("#new_chapters").hide();
			$("#chapters>ul").css("padding-top", "65px");
		}

		//显示章节目录列表
		if (jsonObj.chapters.count > 0) {
			var output_chapters = "";

			$.each(jsonObj.chapters.lists, function (index, chapter) {
				output_chapters += '<li>\n';
				output_chapters += '<a href="article.html?bid=' + chapter.bid + '&article_id=' + chapter.article_id + '&title=' + title + '">' + chapter.article_title + '</a>\n';
				output_chapters += '</li>\n';
			});

			$("#chapters>ul").html(output_chapters);
		}

		//列出跳转页面目录列表
		if (jsonObj.total_page_count > 1) {
			var output_pages = "";

			for (var count = 1; count <= jsonObj.total_page_count; count++) {
				output_pages += '<div class="selection" page="' + count + '">第' + ((count-1)*20+1) + ' - ' + count * 20 + '章</div>\n';
			}

			$("#jumpto").html(output_pages);
			$("#jump").html("跳转");
			$("#jump").css("color", "white");

			$(".selection").off("mousedown");
			$(".selection").each(function () {
				$(this).mousedown(function () {
					getBookHomeChapters($(this).attr("page"));
				});
			});
		} else {
			$("#jumpto").empty();
			$("#jump").html("NULL");
			$("#jump").css("color", "black");
		}

		if (jsonObj.prev_page) {
			$("#prev_page").off("click");
			$("#prev_page").click(function () {
				getBookHomeChapters(jsonObj.prev_page);
			});

			$("#prev_page").html("上一页");
			$("#prev_page").css("color", "white")
		} else {
			$("#prev_page").html("NULL");
			$("#prev_page").css("color", "black")
		}

		if (jsonObj.next_page) {
			$("#next_page").off("click");
			$("#next_page").click(function () {
				getBookHomeChapters(jsonObj.next_page);
			});

			$("#next_page").html("下一页");
			$("#next_page").css("color", "white")
		} else {
			$("#next_page").html("NULL");
			$("#next_page").css("color", "black")
		}

		$("#postnavi").show();
	} else {
		alert(jsonObj.error_msg);
		location.href = "index.html";
	}
}
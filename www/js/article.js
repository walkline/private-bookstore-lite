'use strick'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				initView();

				break;
			case "backbutton":
				gotoBookHomepage();

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();
				
				break;
		}
	}
};

var uid = localStorage.uid;
var bid = $.url().param("bid");
var article_id = $.url().param("article_id");
var title = $.url().param("title");

window.onscroll = function () {
	//if ($(document).scrollTop() + $(window).height() >= $(document).height()) {
	if ($(window).scrollTop() >= $(document).height() - $(window).height() - 50){
		console.log("1");
		$("#postnavi").css("opacity", "1");
	} else {
		console.log("0.5");
		$("#postnavi").css("opacity", "0.5");
	}

	saveLastPos();
}

$("#homepage").click(function () {
	location.href = "index.html";
});

$("#catalog").click(function () {
	gotoBookHomepage();
});

function gotoBookHomepage() {
	location.href = "home.html?&bid=" + bid + "&title=" + title;
}

function initView() {
	if (!uid) {
		location.href = "index.html";
	} else {
		$(".ptm-title").html(title);

		getArticleContents();
	}
}

function getArticleContents() {
	$.post (getApiUrl("articlecontents"), {bid: bid, article_id: article_id}, function (result) {
		generateArticleContents(result);
	});
};

function generateArticleContents(jsonObj) {
	if (!jsonObj.error_code) {
		$("#article_title").html(jsonObj.article_title);
		$("#article_content").html(jsonObj.article_content);

		if (jsonObj.prev_article_id) {
			$("#prev_article").html('<a href="article.html?bid=' + bid + '&article_id=' + jsonObj.prev_article_id + '&title=' + title + '">上一章</a>');
			$("#prev_article").css("color", "white")
		} else {
			$("#prev_article").html("NULL");
			$("#prev_article").css("color", "black")
		}

		if (jsonObj.next_article_id) {
			$("#next_article").html('<a href="article.html?bid=' + bid + '&article_id=' + jsonObj.next_article_id + '&title=' + title + '">下一章</a>');
			$("#next_article").css("color", "white")
		} else {
			$("#next_article").html("NULL");
			$("#next_article").css("color", "black")
		}

		$("#postnavi").show();
		getset();
		scroll2LastPos();

		$.post(getApiUrl("updatehistory"), {uid: uid, bid: bid, article_title: jsonObj.article_title, article_id: article_id});
	} else {
		alert(jsonObj.error_msg);
		gotoBookHomepage();
	}
}
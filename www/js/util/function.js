var arrows = {
	"none": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAABLAAAASwAJArFzAAAAKwSURBVGje7ZdNSFRRFMd/r5lJ06HCspKMnELpU4gyiCgqadMXRVJREYRgUy1KWkSBTBK1KdoUEu2iKCKSPjbaB1FEkQWluwyLUDODFLQZP8fbwsdp9D115L0ZN/e3mnfvued/5sy9//sGNBqNRqPRaCYYTxwxM1hBlM5xZs4iny4izkucTxVhPrJ5XKsK+UCYanKcF1CCQqFoZG9c/QIPRfwwVx1xXsAeesxk7ZQyecx4Hyf4Y67oYZ/zAvxcpddMGKYc/6jR6YT4a0b3cm2M6DhJ5wIRSXqdzBEjZ1Ih/Ypw0R15gBRO0mYmjnKfbNuoudwjGvNzpbglD+DhAD/N5IoX5FsilvJU5ls4GOeGHQcG2/giErWsHTK7hk8yV892DLflBymgRmS+iozBFuplvIbViREfZDHPROoXh/Hg4RAtMvacJYmUB8jmDv2y1U5RSrv51M9d5iVaHiCDCnGGbrrNT31UkJEMeQA/ZXRI2xWKDkLunXorXsuu9hIUw1W0cRTfsAgDrzviqRyjknPMHjY+if00oVA02Zz6WYSo5DhTnBewkzCKAR6z0DK3iQdUUmgZD/CQKIoIu5wXUCKNfmPjfz5L62E5r2VN0HkBeTEeV8vGMePX81ni61jkvABYxTtJ2UjRKDZrsFteRVz1xAU8YsBM+5sSm7YDeCmm1Ywa4Am5bskDzOGm+F8nZaRZItI4K97Qz22y3JQHmMYlusT/rjB9yOxULosnWmddIpXTdNr6fja36JP+nHHj9Nvjozjm5qsmD4BcqmSsleAIO8QlDLbSIHLvKWAlb+X5OzsS9SoSy7qYk94QU04dGxIvPsgyXg65CxWKVzY+mUByTLf/f+oDyZQHyJT/AL3csNyVScFPOc00c97Jq4izPesjgME3+ibi+2s0Go1Go3GJf+QWQl14E+2HAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE1LTA3LTI1VDIxOjQ5OjI3KzA4OjAw56k66QAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNC0wNC0wNlQwOTozOTo1MyswODowMLOia0AAAABOdEVYdHNvZnR3YXJlAEltYWdlTWFnaWNrIDYuOC44LTEwIFExNiB4ODZfNjQgMjAxNS0wNy0xOSBodHRwOi8vd3d3LmltYWdlbWFnaWNrLm9yZwUMnDUAAAAjdEVYdHN2Zzpjb21tZW50ACBHZW5lcmF0b3I6IEljb01vb24uaW8gvDCsgAAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABh0RVh0VGh1bWI6OkltYWdlOjpIZWlnaHQANTMzyrwBlQAAABd0RVh0VGh1bWI6OkltYWdlOjpXaWR0aAA1MzNZTVHIAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADEzOTY3NDgzOTNTu/4AAAAAE3RFWHRUaHVtYjo6U2l6ZQA0LjkyS0JCgUAF7AAAAFp0RVh0VGh1bWI6OlVSSQBmaWxlOi8vL2hvbWUvd3d3cm9vdC93d3cuZWFzeWljb24ubmV0L2Nkbi1pbWcuZWFzeWljb24uY24vc3JjLzExNDMyLzExNDMyNjYucG5nyq/cqwAAAABJRU5ErkJggg==)",
	"block": "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAABLAAAASwAJArFzAAAAKoSURBVGje7ZdLSFRRGMd/4/iIprTBcKboIVgIFRZpm0zIKCgTiggJamFFUVBghEGUlFSLXhgWtQiJkFpEFEYLC6KICmsRWTQ9TBcWWmaWlQ5jo7eFh49rM+rYvTOzOf+7mXvOd8/vP/c8vu+ClpaWlpaWVpzltPh8Om56MeJjPoG1PKWJA7jigU9iGx0YGAQ4R3qs8eM5yC8MdfVzg5mxxKdxmoDgB697zIkVfhpXCCrsT9rEwnPyY4GfTb0g29lCPs/k/gNF0cYvokFwLRTjALK5K21tlJIYPfwK3gqqkQJpn8E106TsY1w04E5KaBX8feYN6XVzRpaln+Ok2o1PpowuBRigjsyQCBeH+K0i/nAJj514F5X0qMH7OE9G2KgkdvJVTN4MY/I/NZkL9KmBeznGhGEjEyjhk0zTY3LtwKdSy4Aasos9oy6w5bwyLdQc6waK8Muu3xRR9pzPE7FwwrqBQroxMHhHcUhfJpVUsySkfRa31VursG4gmXLecIu8MP/0IQYGrZSQ8E+flyreU8NU6wbAiSdMxl9Mo7zoTraHnH9JeKNzIAE4WE3TkFzYzf7YlSVOSvks+71f/QpwFncs8CmU8V1Bg1zlMD/krtaeOR9JaZyi13QmunGylS8yFfVkRxOfQY0p61WoM9HBGprFQgMLo4XPok5mvJMdQ9Z9AS/Fgo9VOOzH5/BIEOF2/gIemMqSDSH9FrWUFzL8a5aFjZnOdcka39hNsl1wB+v5aJrjvGEjM7goebOHI/acDA42qw+QwVIka8ToiRyVyiFAtR2VUZ4U3kEu4x01PoW9ppNhl3UD69RL9XOSSRE9kchGKUuqrBvwcIcgHZSPKbWsxEeQFgpHC4xkv04hl3YaCY7JeBZzacYXr093LS0tLS0tLa2I9RcPRUP/lN/6hgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxNS0wNy0yNVQyMTo0OToyNyswODowMOepOukAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTQtMDQtMDZUMDk6Mzk6NTkrMDg6MDAX0jQOAAAATnRFWHRzb2Z0d2FyZQBJbWFnZU1hZ2ljayA2LjguOC0xMCBRMTYgeDg2XzY0IDIwMTUtMDctMTkgaHR0cDovL3d3dy5pbWFnZW1hZ2ljay5vcmcFDJw1AAAAI3RFWHRzdmc6Y29tbWVudAAgR2VuZXJhdG9yOiBJY29Nb29uLmlvILwwrIAAAAAYdEVYdFRodW1iOjpEb2N1bWVudDo6UGFnZXMAMaf/uy8AAAAYdEVYdFRodW1iOjpJbWFnZTo6SGVpZ2h0ADUzM8q8AZUAAAAXdEVYdFRodW1iOjpJbWFnZTo6V2lkdGgANTMzWU1RyAAAABl0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL3BuZz+yVk4AAAAXdEVYdFRodW1iOjpNVGltZQAxMzk2NzQ4Mzk5s24XHgAAABN0RVh0VGh1bWI6OlNpemUANS4wNktCQqVF+34AAABadEVYdFRodW1iOjpVUkkAZmlsZTovLy9ob21lL3d3d3Jvb3Qvd3d3LmVhc3lpY29uLm5ldC9jZG4taW1nLmVhc3lpY29uLmNuL3NyYy8xMTQzMi8xMTQzMjc1LnBuZ0ZTdd4AAAAASUVORK5CYII=)"
};

/**
* 初始化小说网站模块折叠状态
*
* 调用来自: index.interface, manage.interface
*/
function init() {
	var headers = new Array("book_header");
	var lists = new Array("book_lists");

	for (var count = 0; count < headers.length; count++)
	{
		var status = getCookie(headers[count] + "_status");

		if (status != null) {
			document.getElementById(lists[count]).style.display = status;
			document.querySelector("#" + headers[count] + ">span").style.backgroundImage = arrows[status];
		} else {
			document.getElementById(lists[count]).style.display = "block";
		}
	}
}

function getApiUrl(command) {
	var url = "";
	var prefix = isLocale() ? "http://localhost/books/api/" : "http://walkline.wang/books/api/";

	switch (command) {
		case 'getbooklists':
			url = prefix + "util/v1/getbooklists";

			break;
		case 'appendbook':
			url = prefix + "util/v1/appendbook";

			break;
		case 'removebook':
			url = prefix + "util/v1/removebook";

			break;
		case 'updatehistory':
			url = prefix + "util/v1/updatehistory";

			break;
		case 'catalogfilters':
			url = prefix + "v1/catalogfilters";

			break;
		case 'bookinfo':
			url = prefix + "v1/bookinfo";

			break;
		case 'bookchapters':
			url = prefix + "v1/bookchapters";

			break;
		case 'articlecontents':
			url = prefix + "v1/articlecontents";

			break;
		case 'checkupdate':
			url = prefix + "util/v1/checkupdate";

			break;
		case 'weibo_login':
			url = prefix + "util/oauth2/weibo/login.php";
	}

	return url;
}

function isLocale() {
	return location.hostname == "localhost" ? true : false;
}

/**
* 获取当前访问URL的参数
*
* 调用来自: article.interface, function.js->saveLastPos()
*			, function.js->saveLastPos(), function.js->doAppend()
*			, function->doImport()
*/
function getArgs() {
    var args = {};
    var match = null;
    var search = decodeURIComponent(location.search.substring(1));
    var reg = /(?:([^&]+)=([^&]+))/g;

    while ((match = reg.exec(search)) !== null) {args[match[1]] = match[2];}

    return args;
}

/**
* 滚动到上次浏览的位置
*
* 位置信息存储于cookie中，由bid_page区分
* 调用来自: article.interface
*/
function scroll2LastPos() {
	var bid = $.url().param("bid");
	var article_id = $.url().param("article_id");
	var cookie_id = bid + "_" + article_id;
	var pos = getCookie(cookie_id);

	if (pos) {$(document).scrollTop(pos);}
}

/**
* 记录页面浏览位置
*
* 位置信息存储于cookie中，由bid_page区分
* 调用来自: article.interface
*/
function saveLastPos() {
	var bid = $.url().param("bid");
	var article_id = $.url().param("article_id");
	var cookie_id = bid + "_" + article_id;
	var pos = $(document).scrollTop();

	setCookie(cookie_id + "=" + pos);
}

/**
* 从cookie中读取指定信息
*
* @param id 指定信息的id
* @return 指定信息的value
*/
function getCookie(id) {
	var arr = {};
	var reg = new RegExp("(^| )" + id + "=([^;]*)(;|$)");

	if (arr = document.cookie.match(reg)) {
		return unescape(arr[2]);
	} else {
		return null;
	}
}

/** 
* 保存cookie信息，并设置过期时间为30天
*
* @param string 需要保存的id和value的拼接字符串
*/
function setCookie(string) {
	if (string.length == 0) {return;}

	var date = new Date();
	var expireDay = 30;

	date.setDate(date.getDate() + expireDay);
	document.cookie = string + "; expires=" + date.toUTCString();
}

//用于折叠index.interface和manage.interface中的小说网站模块
function hideList(header) {
	switch (header.id) {
		case "book_header":
			var oContent = $("#book_lists");
			break;
	}

	if (oContent.css("display") == "none") {
		setCookie(header.id + "_status=block");
		document.querySelector("#"+header.id+">span").style.backgroundImage = arrows["block"];
	} else {
		setCookie(header.id + "_status=none");
		document.querySelector("#"+header.id+">span").style.backgroundImage = arrows["none"];
	}

	$(oContent).slideToggle();
}

function getYear() {document.write(new Date().getFullYear());}
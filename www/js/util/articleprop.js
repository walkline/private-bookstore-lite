/* 以下函数用于article.html中关于页面字体、背景颜色等的设置 */

var checkbg = "#A7A7A7";

//内容页用户设置
function nr_setbg(intype) {
	var huyandiv = document.getElementById("huyandiv");
	var light = document.getElementById("lightdiv");
	
	if (intype == "huyan") {
		if (huyandiv.style.backgroundColor == "") {
			set("light","huyan");
			setCookie("light=huyan;path=/");
		} else {
			set("light","no");
			setCookie("light=no;path=/");
		}
	} else if (intype == "light") {
		if (light.innerHTML == "关灯") {
			set("light","yes");
			setCookie("light=yes;path=/");
		} else {
			set("light","no");
			setCookie("light=no;path=/");
		}
	} else if (intype == "big") {
		set("font","big");
		setCookie("font=big;path=/");
	} else if (intype == "middle") {
		set("font","middle");
		setCookie("font=middle;path=/");
	} else if (intype == "small") {
		set("font","small");
		setCookie("font=small;path=/");
	}			
}

//内容页读取设置
function getset() {
	var strCookie=document.cookie;
	var arrCookie=strCookie.split("; ");
	var light;
	var font;

	for (var i=0; i < arrCookie.length; i++) {
		var arr = arrCookie[i].split("=");
		if ("light" == arr[0]) {
			light = arr[1];
			break;
		}
	}

	for (var i=0; i < arrCookie.length; i++) {
		var arr = arrCookie[i].split("=");
		if ("font" == arr[0]) {
			font = arr[1];
			break;
		}
	}
	
	//light
	if (light == "yes") {
		set("light","yes");
	} else if (light == "no") {
		set("light","no");
	} else if (light == "huyan") {
		set("light","huyan");
	}
	//font
	if (font == "big") {
		set("font","big");
	} else if (font == "middle") {
		set("font","middle");
	} else if (font == "small") {
		set("font","small");
	} else {
		set("","");	
	}
}

//内容页应用设置
function set(intype, p) {
	var article_body = $("body"); //页面body
	var huyandiv = $("#huyandiv"); //护眼div
	var lightdiv = $("#lightdiv"); //灯光div
	var fontbig = document.getElementById("fontbig"); //大字体div
	var fontmiddle = document.getElementById("fontmiddle"); //中字体div
	var fontsmall = document.getElementById("fontsmall"); //小字体div
	var article_content = $("#article_content"); //正文内容div
	var article_title = $("#article_title"); //文章标题
	
	//灯光
	if (intype == "light") {
		if (p == "yes") {	
			//关灯
			lightdiv.html("开灯");
			article_body.css("background", "#000");
			huyandiv.css("background", "");
			article_title.css("color", "#999");
			article_content.css("color", "#999");
		} else if (p == "no") {
			//开灯
			lightdiv.html("关灯");
			article_body.css("background", "#f4f4f4");
			article_content.css("color", "#000");
			article_title.css("color", "#000");
			huyandiv.css("background", "");
		} else if (p == "huyan") {
			//护眼
			lightdiv.html("关灯");
			huyandiv.css("background", checkbg);
			article_body.css("background", "#a8e2a5");
			article_title.css("color", "#000");
			article_content.css("color", "#000");
		}
	}
	//字体
	if (intype == "font") {
		fontbig.style.backgroundColor = "";
		fontmiddle.style.backgroundColor = "";
		fontsmall.style.backgroundColor = "";
		if (p == "big") {
			fontbig.style.backgroundColor = checkbg;
			article_content.css("font-size", "20px");
		} else if (p == "middle") {
			fontmiddle.style.backgroundColor = checkbg;
			article_content.css("font-size", "18px");
		} else if (p == "small") {
			fontsmall.style.backgroundColor = checkbg;
			article_content.css("font-size", "16px");
		}
	}
}
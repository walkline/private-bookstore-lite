'use strict'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				initView(localStorage.settings_paging_limit);

				$("#weibo_login").click(function () {weibo_login();});

				break;
			case "backbutton":
				navigator.app.exitApp();

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();

				break;
		}
	}
};

var uid = localStorage.uid;

function initView(limit, page) {
	limit = limit || 5;
	page = page || 0;

	if (!uid) {
		$("#weibo").show();
	} else {
		$.post (getApiUrl("getbooklists"), {uid: uid, limit: limit, page: page}, function (result) {
			$("#book_lists").html(generateLists(result));
		});
	}

	if (!localStorage.settings_clean_search_result) {localStorage.settings_clean_search_result = true;}
	if (!localStorage.settings_clean_history) {localStorage.settings_clean_history = true;}
	if (!localStorage.settings_paging_limit) {localStorage.settings_paging_limit = 0;}
	if (!localStorage.settings_show_new_chapter) {localStorage.settings_show_new_chapter = true;}
	if (!localStorage.settings_new_chapter_limit) {localStorage.settings_new_chapter_limit = 12;}
	if (!localStorage.settings_chapter_sort_order) {localStorage.settings_chapter_sort_order = "asc";}
};

function generateLists(jsonObj) {
	var output = "";

	if (jsonObj.lists.length > 0) {
		//小说列表和阅读历史记录
		$.each(jsonObj.lists, function(index, list) {
			output += '<div class="s_list">\n';
			output += '<a href="./home.html?bid=' + list.bid + '&title=' + list.title + '">' + list.author + '：《' + list.title + '》</a>\n';

			if (!$.isEmptyObject(list.history)) {
				var history = list.history;

				output += '<a class="history" href="article.html?bid=' + list.bid + '&article_id=' + history.article_id + '&title=' + list.title + '">　└当前阅读：《' + history.article_title + '》</a>\n';
			}

			output += '</div>\n';
		});

		//翻页区
		if (!$.isEmptyObject(jsonObj.paging)) {
			var paging = jsonObj.paging;

			output += '<div class="s_list" style="height:30px;margin-top:0px;">\n';

			for (var count = 0; count < paging.paging_counts; count++) {
				var active_a = count == paging.current_page ? 'style="background-color:black;"' : "";
				var active_span = count == paging.current_page ? 'style="color:white;"' : "";

				output += '<a class="nav_pages" ' + active_a + ' href="javascript:void(0);" onclick="initView(' + localStorage.settings_paging_limit + ', ' + count + ')">\n';
				output += '<span class="nav_span" ' + active_span + '>' + (count + 1) +'</span>\n';
				output += '</a>';
			}

			output += '</div>';
		}
	} else {
		output = '<a href="manage.html" class="prompt">书架里空空如也，赶紧去选一本书吧</a>';
	}

	return output;
}

function weibo_login() {
	$.post (getApiUrl("weibo_login"), {}, function (result) {
		var login_frame = "<iframe id='login_content' style='z-index:1000;height:100%;width:100%;border:0 none;position:fixed;top:0px;left:0px;' src='" + result + "'></iframe>";

		$(document.body).after(login_frame);
	});
}

window.onmessage = function (e) {
	var json_data = JSON.parse(e.data);

	if (json_data.result === "success") {
		localStorage.uid = json_data.uid;
		localStorage.access_token = json_data.access_token;

		$("#login_content").remove();

		location.href = "index.html";
	} else {
		$("#login_content").remove();
	}
}

$(window).scroll(function () {
	var scrollY = $(document).scrollTop();

	if (scrollY > 60) {
		$("#header-bar").addClass("opacity90");//.slideUp();
	} else {
		$("#header-bar").removeClass("opacity90");//.show()
	}
});
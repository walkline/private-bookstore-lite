'use strict'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				checkUpdate();

				break;
			case "backbutton":
				navigator.app.exitApp();

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();

				break;
		}
	}
};

//版本号，用于检查更新
//不要忘记同时修改根目录下config.xml中的版本信息
var app_code = "private-bookstore-lite";
var version_code = 100;

$("#webhomepage").click(function () {
	window.open("http://walkline.wang", "_system", "location=yes");
});

$("#bug_header").click(function () {
	window.open("http://walkline.wang/bbs/index.php/node/show/3", "_system", "location=yes");
});

function checkUpdate() {
	$.post(getApiUrl("checkupdate"), {app_code: app_code, version_code: version_code}, function (result) {
		if (!$.isEmptyObject(result)) {
			var msg = "";

			for (var count = 0; count < result.msg.length; count++) {
				msg += "<ul>\n";
				msg += "<li>" + result.msg[count] + "</li>\n";
			}
			msg += "</ul>";

			$("#update_header").html("发现新版本 - Version " + result.version_name);
			$("#update_lists").html(msg);
			$(".update").show();
			$("#update_now").attr("onclick", "updateNow('" + result.url + "')");
			//稍后添加更新说明信息
		}
	});
}

function updateNow(url) {
	cordova.InAppBrowser.open(url, "_system", "location=no, hidden=yes, zoom=no");
}
'use strict'

var app = {
	initialize: function () {
		this.bindEvents();
	},

	bindEvents: function () {
		document.addEventListener('deviceready', this.onDeviceReady, false);
		document.addEventListener("backbutton", this.onBackButton, false);
		document.addEventListener("menubutton", this.onMenuButton, false);
		document.addEventListener("offline", this.onOffLine, false);
		document.addEventListener("online", this.onOnLine, false);
	},

	onDeviceReady: function () {
		app.receivedEvent('deviceready');
	},

	onBackButton: function () {
		app.receivedEvent('backbutton');
	},

	onMenuButton: function () {
		app.receivedEvent('menubutton');
	},

	onOnLine: function () {
		app.receivedEvent('online');
	},

	onOffLine: function () {
		app.receivedEvent('offline');
	},

	receivedEvent: function(id) {
		switch (id) {
			case "deviceready":
				initView();

				break;
			case "backbutton":
				navigator.app.exitApp();

				break;
			case "menubutton":
				alert("menubutton clicked");

				break;
			case "online":
				$("#networkerror").fadeOut();
				
				break;
			case "offline":
				$("#networkerror").fadeIn();

				break;
		}
	}
};

var url;
var uid = localStorage.uid;

//响应注销命令
$("#logout").click(function () {
	localStorage.clear();
	location.href = "index.html";	
});

//设置项
$("#settings_paging_limit").change(function () {
	localStorage.settings_paging_limit = $(this).val();
});

$("#settings_clean_history").change(function () {
	localStorage.settings_clean_history = $(this).is(":checked");
});

$("#settings_clean_search_result").change(function () {
	localStorage.settings_clean_search_result = $(this).is(":checked");
});

$("#settings_show_new_chapter").change(function () {
	localStorage.settings_show_new_chapter = $(this).is(":checked");
});

$("#settings_new_chapter_limit").change(function () {
	localStorage.settings_new_chapter_limit = $(this).val();
});

$(".ptm-card-header").click(function () {
	$(this).next().slideToggle();
});

function initView() {
	if (!uid) {
		location.href = "index.html";
	} else {
		initSettings();
	}
};

function initSettings() {
	$("#settings_paging_limit option[value='" + localStorage.settings_paging_limit + "']").attr("selected", true);
	$("#settings_clean_history").prop("checked", localStorage.settings_clean_history != "false");
	$("#settings_clean_search_result").prop("checked", localStorage.settings_clean_search_result != "false");
	$("#settings_show_new_chapter").prop("checked", localStorage.settings_show_new_chapter != "false");
	$("#settings_new_chapter_limit option[value='" + localStorage.settings_new_chapter_limit + "']").attr("selected", true);
}
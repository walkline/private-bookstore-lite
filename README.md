# 私人书库 Lite

私人书库 Lite是一个小说阅读App，是使用[PhoneGap CLI][phonegap-cli-url]框架开发并打包的安卓应用，小说资源使用[Book API][book-api]获取，后台数据交换使用[私人书库 Lite - Server][private-bookstore-lite_server]，[Book API][book-api]和[私人书库 Lite - Server][private-bookstore-lite_server]是使用PHP编写的RESTful API程序

> [Book API 在线演示][book-api-sample]

## 软件截图

> 首页


![][screenshot-01]

> 小说目录


![][screenshot-02]

> 小说正文


![][screenshot-03]

> 管理页面


![][screenshot-04]

> 设置页面


![][screenshot-05]

> 关于页面


![][screenshot-06]

## 功能介绍

#### 用户登录

App不使用单独的用户管理系统，仅支持微博账号登录。

*`使用微博账号登录时，微博安全中心会发出一条登录提醒，并非安全问题，介意者请勿使用`*

## 问题反馈

如果在使用中遇到什么，你可以：

- 提交Issues
- 登录[我的论坛][bbs-issue-report]，发帖留言

## 更新日志

	2017-06-02
		1、移除小说正文底部空行
		2、自动调整小说正文页导航条透明度
		3、发布 Release v1.0.1

	2017-05-21
		1、完成小说内容获取并显示功能
		2、发布 Release v1.0.0

	2017-05-20
		完成（小说首页）章节目录获取并显示功能

	2017-05-19
		1、添加关于页，并提供检查更新功能
		2、优化（管理页）控件样式

	2017-05-18
		完成（管理页）添加指定小说

	2017-05-16
		1、完成（管理页）删除指定小说功能
		2、增加设置功能
		3、完成（首页）小说列表获取并显示功能

	2017-05-14
		创建项目

## [PhoneGap 插件][phonegap-plugin-apis]

在打包之前，需要先增加如下插件：

	phonegap plugin add cordova-plugin-whitelist
	phonegap plugin add cordova-plugin-network-information
	phonegap plugin add cordova-plugin-inappbrowser
	phonegap plugin add cordova-plugin-dialogs

[private-bookstore-lite_server]: http://git.oschina.net/walkline/private-bookstore-lite_server
[book-api]: http://walkline.wang/books/api/v1
[book-api-sample]: http://walkline.wang/books/api/v1/sample/
[phonegap-cli-url]: http://github.com/phonegap/phonegap-cli
[phonegap-plugin-apis]: http://docs.phonegap.com/references/plugin-apis
[bbs-issue-report]: http://walkline.wang/bbs/index.php/node/show/3
[screenshot-01]: screenshot/screenshot-01.png
[screenshot-02]: screenshot/screenshot-02.png
[screenshot-03]: screenshot/screenshot-03.png
[screenshot-04]: screenshot/screenshot-04.png
[screenshot-05]: screenshot/screenshot-05.png
[screenshot-06]: screenshot/screenshot-06.png